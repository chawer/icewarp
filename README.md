## Technologies

* React 0.13
* ES6, ES7 & JSX to ES5 via babel
* webpack with react hot loader, and other useful loaders
* [Local CSS](https://github.com/webpack/css-loader#local-scope)
* Karma, mocha, chai & sinon for testing
* Basic flux architecture with app actions and stores
* React router ([feature/react-router](https://github.com/badsyntax/react-seed/tree/feature/react-router))
* Material UI ([feature/material-ui](https://github.com/badsyntax/react-seed/tree/feature/material-ui))

## npm scripts

* `npm start` - Build and start the app in dev mode at http://localhost:8000
* `npm test` - Run the tests
* `npm run build` - Run a production build


## Credits

This project was initially forked from React seed - https://github.com/badsyntax/react-seed 

## License

Copyright (c) 2015 Richard Willis

MIT (http://opensource.org/licenses/MIT)
