import AppDispatcher from '../dispatcher/AppDispatcher';
import GetXML from '../util/GetXML';
import SeriesGenerator from '../util/SeriesGenerator';

import {
  CALENDAR_SERIES_GET_SUCCESS,
  CALENDAR_SERIES_UPDATED,
  CALENDAR_SERIES_GET_ERROR
} from '../constants/AppConstants';

const actions = {
  getXml(path) {
    GetXML.makeXMLRequest(path)
    .then((data) => {
      let generator = new SeriesGenerator(data); // make singleton
      let beforeParsing = new Date();
      let series = generator.get();
      let parsingTime = (new Date()) - beforeParsing;
      series['parsingTime'] = parsingTime;

      AppDispatcher.dispatch({
        actionType: CALENDAR_SERIES_GET_SUCCESS,
        series: series
      });
    })
    .catch(() => {
      AppDispatcher.dispatch({
        actionType: CALENDAR_SERIES_GET_ERROR
      });
    });
  },
}

export default actions;
