import BaseStore from './BaseStore';
import AppDispatcher from '../dispatcher/AppDispatcher';

import {
  CALENDAR_SERIES_GET_SUCCESS,
  CALENDAR_SERIES_UPDATED,
  CALENDAR_SERIES_GET_ERROR
} from '../constants/AppConstants';

class CalendarItemsStore extends BaseStore {

  emitChange() {
    this.emit(CALENDAR_SERIES_UPDATED);
  }

  addChangeListener(callback) {
    this.on(CALENDAR_SERIES_UPDATED, callback);
  }

  removeChangeListener(callback) {
    this.removeListener(CALENDAR_SERIES_UPDATED, callback);
  }
}

let store = new CalendarItemsStore();

AppDispatcher.register((action) => {
  switch(action.actionType) {
    case CALENDAR_SERIES_GET_SUCCESS:
      store.setAll(action.series);
      break;
    default:
  }
});

export default store;
