import { EventEmitter } from 'events';

export default class BaseStore extends EventEmitter {

  constructor(...args) {
    super(...args);
    this.data = {};
  }

  setAll(items) {
    this.data = items;
    this.emitChange();
  }

  getAll() {
    return this.data;
  }
}
