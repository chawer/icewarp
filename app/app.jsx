import './favicon.ico';
import './index.html';
import './icewarp.xml';
import './icewarp2.xml';
import './test1.xml';
import 'babel-core/polyfill';
import 'normalize.css/normalize.css';
import './scss/app.scss';

import React from 'react';
import App from './components/App/App';

React.render(
  <App />,
  document.getElementById('app')
);
