import styles from './_XmlLoader.scss';
import React from 'react';
import moment from 'moment';
import AppActions from '../../actions/AppActions';

let { PropTypes } = React;

export default class XmlLoader extends React.Component {
  constructor() {
    super();

    this.state = {
      inputVal: 'test1.xml',
      parsedXml: '',
    };
  }

  static propTypes = {
    parsingTime: PropTypes.number,
  };

  handleInputChange(event) {
    this.setState({inputVal: event.target.value});
  }

  handleSubmit(event) {
    event.preventDefault();
    AppActions.getXml(this.state.inputVal);
    this.setState({parsedXml: this.state.inputVal});
  }

  render() {
    return (
      <div className={styles.XmlLoader}>
        <form onSubmit={this.handleSubmit.bind(this)}>
          <label>
            XML: <input type="text" value={this.state.inputVal} onChange={this.handleInputChange.bind(this)} />
          </label>
          <input type="submit" value="Load" />
        </form>
        <span className={styles.loaderHelp}>
          {this.props.parsingTime ? <i>{this.state.parsedXml} was parsed in {this.props.parsingTime}ms. </i> : null }
          <i>Try icewarp.xml, icewarp2.xml as well.</i>
        </span>
      </div>
    )
  }
}
