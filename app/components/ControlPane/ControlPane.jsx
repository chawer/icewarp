import styles from './_ControlPane.scss';
import React from 'react';
import XmlLoader from './XmlLoader'

let { PropTypes } = React;

export default class ControlPane extends React.Component {
  static propTypes = {
    parsingTime: PropTypes.number,
  };

  render() {
    return (
      <div className={styles.calendarHeader}>
        <XmlLoader parsingTime={this.props.parsingTime} />
      </div>
    )
  }
}
