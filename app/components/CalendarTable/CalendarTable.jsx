import React from 'react';

import styles from './_CalendarTable.scss';
import CalendarHeader from '../CalendarHeader/CalendarHeader';
import EventMarker from '../EventMarker/EventMarker';

let { PropTypes } = React;

export default class CalendarTable extends React.Component {

  static propTypes = {
    series: PropTypes.object.isRequired,
  };

  render() {
    return (
      <table className={styles.tableBody}>
        <CalendarHeader highlightedDay={this.props.series.highlightedDay} weekDays={this.props.series.weekDays} />
        <tbody>
          <tr height="1">
            <td></td>
            <td colSpan="7">
              <div className={styles.spanningwrapper}>
                <div className={styles.hourmarkers}>
                  {
                    [...Array(24)].map((hour, i) =>
                      <div className={styles.markercell} key={`markercell-${i}`}>
                        <div className={styles.dualmarker}>
                        </div>
                      </div>
                    )
                  }
                </div>
              </div>
            </td>
          </tr>
          <tr>
            <td className={styles.timeTd}>
              {
                [...Array(24)].map((hour, i) =>
                  <div className={styles.timing} title={i} key={`timing-${i}`}>
                    {i}<sup>00</sup>
                  </div>
                )
              }
            </td>
            {
              [...Array(7)].map((item, i) =>
                <td className={styles.tgCol} key={`tgCol-${i}`}>
                  <div className={styles.tgColWr}>
                    <EventMarker
                      calendarItems={this.props.series.items}
                      currentDay={i + 1}
                      timeBoxesOrderConfig={this.props.series.timeBoxesOrderConfig}
                    />
                  </div>
                </td>
              )
            }
          </tr>
        </tbody>
      </table>
    );
  }
}
