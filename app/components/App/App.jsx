import styles from './_App.scss';

import React from 'react';
import AppActions from '../../actions/AppActions';
import CalendarItemsStore from '../../stores/CalendarItemsStore';

import CalendarTable from '../CalendarTable/CalendarTable';
import Footer from '../Footer/Footer';
import ControlPane from '../ControlPane/ControlPane';

function getAppState() {
  return {
    series: CalendarItemsStore.getAll()
  };
}

export default class App extends React.Component {

  state = getAppState()

  componentDidMount() {
    CalendarItemsStore.addChangeListener(this.onChange);
  }

  componentWillUnmount() {
    CalendarItemsStore.removeChangeListener(this.onChange);
  }

  onChange = () => {
    this.setState(getAppState());
  }

  render() {
    return (
      <div className={styles.app}>
        <ControlPane parsingTime={this.state.series.parsingTime} />
        
        {Object.keys(this.state.series).length ? (
          <div>
            <CalendarTable series={this.state.series} />
            <Footer />
          </div>
        ) : (
          <div className={styles.notLoaded}>
            <div className={styles.uilCubeCss}><div></div><div></div><div></div><div></div></div>
            <span className={styles.loader}></span>
            <p>Load XML to continue</p>
          </div>
        )}
      </div>
    );
  }
}
