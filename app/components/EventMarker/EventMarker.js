import styles from './_EventMarker.scss';
import React from 'react';
import moment from 'moment';

let { PropTypes } = React;

export default class EventMarker extends React.Component {
  static propTypes = {
    calendarItems: PropTypes.array.isRequired,
    currentDay: PropTypes.number.isRequired,
    timeBoxesOrderConfig: PropTypes.object.isRequired,
  };

  changeBoxAvailability(startBox, numOfOverleapingBoxes) {
    let boxAvailability = this.state.boxAvailability;
    boxAvailability = boxAvailability.map(function(box, i) {
      if (i >= startBox && i <= startBox + numOfOverleapingBoxes) {
        return box++;
      } else {
        return box;
      }
    })
    this.setState(
      {
        boxAvailability: boxAvailability,
      }
    );
  }

  processEvents(events) {
    let processedItems = [];

    events.map((event, i) => {
        let startBox = this.getStartBox(event.startTimestamp);
        let numOfOverleapingBoxes = this.getNumOfOverleapingBoxes(event.startTimestamp, event.endTimestamp);

        let config = {
          startAt: startBox,
          overlapBox: numOfOverleapingBoxes
        };

        let processedItem = Object.assign(event, config); // TODO
        processedItems.push(processedItem);
      });

    this.setState({processedItems: processedItems});
  }

  getItemStyle(item) {
    let size = this.props.timeBoxesOrderConfig[this.props.currentDay][item.id]['size'];
    let width = (84 / size - 2);
    let expandable = this.props.timeBoxesOrderConfig[this.props.currentDay][item.id]['expandableSlotsToRight'];

    if (expandable > 0) {
      width = width * (expandable + 1);
      width += expandable * 2;
    }

    let styles = {
      top: item.startBox * 23,
      height: item.numOfBoxes * 23,
      width: width,
      left: (84 / size) * (this.props.timeBoxesOrderConfig[this.props.currentDay][item.id]['order'] - 1),
    }
    return styles;
  }

  render() {
    let dayEvents = this.props.calendarItems.filter((item) => {
      return item.dayInWeek === this.props.currentDay;
    });

    return (
      <div>
        {
          dayEvents.map((item, i) =>
            <div
              className={styles.calendarItem}
              key={`calendarItem-${i}`}
              style={this.getItemStyle(item)}
              title={`${item.momentStartDate.format('LTS')} - ${item.momentEndDate.format('LTS')}`}
            >
              <span className={styles.calendarTitle}>{item.title}</span>
            </div>
          )
        }
      </div>
    )
  }
}

EventMarker.defaultProps = {
  boxAvailability: Array(48).fill(0),
}
