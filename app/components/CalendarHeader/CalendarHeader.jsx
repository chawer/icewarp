import styles from './_CalendarHeader.scss';
import React from 'react';
import moment from 'moment';

let { PropTypes } = React;

export default class CalendarHeader extends React.Component {
  static propTypes = {
    weekDays: PropTypes.array.isRequired,
    highlightedDay: PropTypes.number.isRequired,
  };

  render() {
    return (
      <thead className={styles.tableHeader}>
        <th title=''>
          /
        </th>
        {
          this.props.weekDays.map((day, i) =>
            <th title={day.name} key={`thCol-${i}`} className={`
                ${styles.calendarTitle}
                ${day.dayOfYear === this.props.highlightedDay ? styles.calendarTitleActive : ''}
              `}>
              {day.name}
            </th>
          )
        }
      </thead>
    );
  }
}
