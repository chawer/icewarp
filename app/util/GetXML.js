import parseXML from 'xml2js';

export default {
  makeXMLRequest(uri) {
    let promise = new Promise(function(resolve, reject) {
      let xhr = new XMLHttpRequest();
      xhr.open('GET', uri, true);
      xhr.send();
      xhr.onreadystatechange = function() {
        if (xhr.readyState === 4) {
         if (xhr.status === 200) {
            let response = xhr.responseText;
            parseXML.parseString(response, {ignoreAttrs: true}, function (err, result) {
              resolve(result);
              console.log(err);
            });
         } else {
            reject(xhr.status);
            console.log("xhr failed");
         }
        } else {
           console.log("xhr processing going on");
        }
      }
    });
    return promise;
  }
}
