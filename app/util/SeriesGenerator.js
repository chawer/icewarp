import moment from 'moment';
import DateUtil from '../util/Date';
import ObjectUtil from '../util/ObjectUtil';

export default class SeriesGenerator {
  constructor(data) {
    this.data = data;
    this.series = {
      items: [],
    }
  }

  processSerie(momentStartDate, momentEndDate, item) {
    this.series.items.push({
      year: momentStartDate.year(),
      dayOfYear: momentStartDate.dayOfYear(),
      startBox: DateUtil.getStartBox(momentStartDate),
      numOfBoxes: DateUtil.getNumOfOverleapingBoxes(momentStartDate, momentEndDate),
      title: item.title[0],
      momentStartDate: momentStartDate,
      momentEndDate: momentEndDate,
      dayInWeek: DateUtil.getDayInWeek(momentStartDate),
      id: Math.random().toString(36).substr(2, 5),
    });
  }

  processOverleapingTimerange(momentStartDate, momentEndDate, item) {
    let startDate = momentStartDate.clone();
    let endDate = momentEndDate.clone();

    if (startDate.startOf('day').isSame(endDate.startOf('day'))) {
      this.processSerie(momentStartDate, momentEndDate, item);
    } else {
      let momentEndOfDay = momentStartDate.clone();
      this.processSerie(momentStartDate, momentEndOfDay.endOf('day'), item);
      let momentStartOfNextDay = momentStartDate.clone().add(1, 'day').startOf('day');
      this.processOverleapingTimerange(momentStartOfNextDay, momentEndDate, item);
    }
  }

  processItemSeries() {
    for (let item of this.data.xml.items[0].item) {
      let momentStartDate = moment(new Date(item.start * 1000).toISOString());
      let momentEndDate = moment(new Date(item.end * 1000).toISOString());

      if (momentStartDate.isoWeek() != this.series.shownWeek) {
        if (momentEndDate.isoWeek() != this.series.shownWeek) {
          continue;
        } else {
          momentStartDate = momentEndDate.clone();
          momentStartDate.startOf('isoweek');
        }
      } else if (momentEndDate.isoWeek() != this.series.shownWeek) {
        if (momentStartDate.isoWeek() != this.series.shownWeek) {
          continue;
        } else {
          momentEndDate = momentStartDate.clone()
          momentEndDate.endOf('isoweek');
        }
      }

      if (momentStartDate < momentEndDate) {
        this.processOverleapingTimerange(momentStartDate, momentEndDate, item);
      } else {
        console.log('err');
      }
    }
  }

  processCalendarHeaderSeries() {
    this.series.weekDays = DateUtil.getWeekDays(this.data.xml.dayinweek[0]);
    this.series.highlightedDay = DateUtil.getNumOfDays(this.data.xml.dayinweek[0]);
    this.series.shownWeek = DateUtil.getWeekNumber(this.data.xml.dayinweek[0]);
  }

  getSerieItemById(id) {
    let item = this.series.items.filter(function(item) {
      return item.id === id;
    });

    return item[0];
  }

  getDaysInWeekWithEvent() {
    let daysInWeekWithEvent = {};

    this.series.items.forEach(function(item) {
      if (typeof daysInWeekWithEvent[item.dayInWeek] === 'undefined') {
        daysInWeekWithEvent[item.dayInWeek] = [];
      }
      daysInWeekWithEvent[item.dayInWeek].push(item);
    });

    return daysInWeekWithEvent;
  }

  getEventBoxesByItemId(items) {
    let eventBoxesByItemId = {};

    items.forEach(function(item) {
      for (let i = 0; i <= item.numOfBoxes; i++) {
        let eventLength = [];

        for (let i = 0; i<=item.numOfBoxes; i++) {
          eventLength.push(i + item.startBox);
        }
        eventBoxesByItemId[item.id] = eventLength;
      }
    });

    return eventBoxesByItemId;
  }

  getEventBoxesByBoxId(boxAvailabilityByItemId) {
    let eventBoxesByBoxId = {};

    for(let [id, boxes] of Object.entries(boxAvailabilityByItemId)) {
      for (let box in boxes) {
        if (typeof eventBoxesByBoxId[boxes[box]] === 'undefined') {
          eventBoxesByBoxId[boxes[box]] = [];
        }
        eventBoxesByBoxId[boxes[box]].push(id);
      }
    }

    return eventBoxesByBoxId;
  }

  getSortedEvents(boxAvailabilityByItemId) {
    let sortedEvents = [];

    for(let [boxId, itemIds] of Object.entries(boxAvailabilityByItemId)) {
      sortedEvents.push({[boxId]: itemIds});
    }

    sortedEvents.sort((aBox, bBox) => {
      let aBoxItems = Object.values(aBox)[0];
      let bBoxItems = Object.values(bBox)[0];

      aBoxItems = aBoxItems.length;
      bBoxItems = bBoxItems.length;

      if (aBoxItems < bBoxItems) {
        return -1;
      }

      if (aBoxItems > bBoxItems) {
        return 1;
      }

      let aItem = this.getSerieItemById(Object.keys(aBox)[0]);
      let bItem = this.getSerieItemById(Object.keys(bBox)[0]);

      return bItem.momentStartDate.unix() - aItem.momentStartDate.unix();
    });
    sortedEvents.reverse(); // max to min

    return sortedEvents;
  }

  getItemOrder(item, sortedArr) {
    let positionIndex = sortedArr[item.startBox].indexOf(item);
    return positionIndex;
  }

  getExpandableSlotsToRight(item, eventMap, itemOrder) {
    if (itemOrder === 0) return 0; // not needed

    let itemIds = Array(item.numOfBoxes).fill().map((e,i) => i + item.startBox);
    let eventMapLength = eventMap[item.startBox].length;

    itemOrder++; // check next
    if (itemOrder === eventMapLength) return 0; // performance tweak

    let availableToExpand = 0;
    for (let i = itemOrder; i <= eventMapLength; i++) {
      let availability = 0;

      for (let itemId of itemIds) {
        if (typeof eventMap[itemId][i] === 'undefined' && i < eventMapLength) {
          availability++;
        }
      }

      if (availability === item.numOfBoxes) {
        availableToExpand++;
      } else if (i === itemOrder) {
        return 0; // not possile
      }
    }

    return availableToExpand;
  }

  getSortedArray(arr, sortArr) {
    arr.sort((item, item2) => {
      let motherIndex1 = sortArr.indexOf(item);
      let motherIndex2 = sortArr.indexOf(item2);
      return motherIndex1 - motherIndex2;
    });

    return arr;
  }

  createMap(boxAvailabilityByBoxId) {
    let countOfMaxEvents = 1;

    for (let [id, items] of Object.entries(boxAvailabilityByBoxId)) {
      if ((boxAvailabilityByBoxId[id] || []).length > countOfMaxEvents) {
        countOfMaxEvents = boxAvailabilityByBoxId[id].length;
      }
    }

    let theMap = {};
    for (let [id, items] of Object.entries(boxAvailabilityByBoxId)) {
      theMap[id] = Array(countOfMaxEvents);
    }

    return theMap;
  }

  processBoxAvailability() {
    let timeBoxesOrderConfig = {};
    let daysInWeekWithEvent = this.getDaysInWeekWithEvent();

    for(let [day, items] of Object.entries(daysInWeekWithEvent)) {
      let boxAvailabilityByItemId = this.getEventBoxesByItemId(items);
      let boxAvailabilityByBoxId = this.getEventBoxesByBoxId(boxAvailabilityByItemId);
      let sortedEvents = this.getSortedEvents(boxAvailabilityByItemId);
      let eventMap = this.createMap(boxAvailabilityByBoxId);

      let processedEvents = [];
      let final = {};
      for (let sortedEvent of sortedEvents) {
        let countOfMaxEvents = 1;
        let eventId = Object.keys(sortedEvent)[0];
        let eventBoxes = Object.values(sortedEvent)[0];
        let processingEvents = [];

        if (processedEvents.includes(eventId)) continue; // performance tweak -> already processed

        for (let eventBox of eventBoxes) {
          if ((boxAvailabilityByBoxId[eventBox] || []).length > countOfMaxEvents) {
            countOfMaxEvents = boxAvailabilityByBoxId[eventBox].length;
          }

          for (let availableItemId of boxAvailabilityByBoxId[eventBox]) {
            if (!processingEvents.includes(availableItemId)) processingEvents.push(availableItemId);
          }
        }

        let sortArray = sortedEvents.map(function(item) {
          return Object.keys(item)[0];
        });

        let boxAvailabilityByBoxIdSorted = {};
        for (let eventBox of Object.keys(boxAvailabilityByBoxId)) {
          boxAvailabilityByBoxIdSorted[eventBox] = this.getSortedArray(boxAvailabilityByBoxId[eventBox], sortArray);
        }

        for (let processingEvent of processingEvents) {
          let theItem = this.getSerieItemById(processingEvent);
          let order = this.getItemOrder(theItem, boxAvailabilityByBoxIdSorted)
          let firstAvailable = null;

          for (let i = 0; i <= eventMap[theItem.startBox].length; i++) {
            if (typeof eventMap[theItem.startBox][i] === 'undefined' && typeof eventMap[theItem.startBox + theItem.numOfBoxes][i] === 'undefined') {
              firstAvailable = i;
              break;
            }
          }

          for (let i = 0; i <= theItem.numOfBoxes; i++) {
            eventMap[theItem.startBox + i][firstAvailable] = theItem;
          }
        }

        for (let processingEvent of processingEvents) {
          let theItem = this.getSerieItemById(processingEvent);
          let itemOrder = this.getItemOrder(theItem, eventMap);

          final[processingEvent] = {
            size: countOfMaxEvents,
            order: itemOrder + 1,
            expandableSlotsToRight: this.getExpandableSlotsToRight(theItem, eventMap, itemOrder),
          }
        }
        processedEvents = processedEvents.concat(processingEvents);
      }
      timeBoxesOrderConfig[day] = final;
    }

    this.series.timeBoxesOrderConfig = timeBoxesOrderConfig;
  }

  get() {
    this.processCalendarHeaderSeries();
    this.processItemSeries();
    this.processBoxAvailability();
    return this.series;
  }
}
