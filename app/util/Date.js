import moment from 'moment';

export default {
  getWeekDays(dayInWeekTimeStamp) {
    let weekDay = moment(new Date(dayInWeekTimeStamp * 1000).toISOString());
    let startDayOfWeek = weekDay.subtract(weekDay.weekday(), 'days'); //TODO

    let daysOfWeek = []
    for (let i = 0; i <= 6; i++) {
      daysOfWeek.push({
        name: startDayOfWeek.add(1, 'days').format("ddd D MMMM"),
        dayOfYear: startDayOfWeek.dayOfYear(),
      });
    }

    return daysOfWeek;
  },
  getNumOfDays(dayInWeekTimeStamp) {
    let numOfDays = moment(new Date(dayInWeekTimeStamp * 1000).toISOString()).dayOfYear();
    return numOfDays;
  },
  getStartBox(momentStartDate) {
    let numOfStartBox = momentStartDate.hour() * 2;
    if (momentStartDate.minute() >= 30) numOfStartBox++;

    return numOfStartBox;
  },
  getNumOfOverleapingBoxes(momentStartDate, momentEndDate) {
    let timeBetween = (momentEndDate.unix() - momentStartDate.unix()) / 3600;
    let numberOfBoxes = (Math.round(timeBetween * 2));

    return numberOfBoxes;
  },
  getDayInWeek(momentStartDate) {
    let dayInWeek = momentStartDate.isoWeekday();
    return dayInWeek;
  },
  getWeekNumber(dayInWeekTimeStamp) {
    let weekNumber = moment(new Date(dayInWeekTimeStamp * 1000).toISOString()).isoWeek();
    return weekNumber;
  },
}
